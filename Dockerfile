# First Stage: Pull Tomcat image from Harbor registry using SHA256 digest
FROM 192.168.0.93/myproject/tomcat:latest

COPY server.xml /usr/local/tomcat/conf/


# Expose port 5000 for documentation purposes
EXPOSE 5000

# Create directories for host-manager and manager
RUN mkdir -p /usr/local/tomcat/webapps/host-manager
RUN mkdir -p /usr/local/tomcat/webapps/manager

# Copy the contents of host-manager and manager to the respective directories
COPY host-manager /usr/local/tomcat/webapps/host-manager
COPY manager /usr/local/tomcat/webapps/manager
COPY tomcat-users.xml /usr/local/tomcat/conf



# Replace with the actual path of your project files
RUN mkdir -p /usr/local/tomcat/webapps/todo-node

COPY . /usr/local/tomcat/webapps/todo-node

# Set the default command to run Tomcat
CMD ["catalina.sh", "run"]

